import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  StatusBar
} from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducer/Index'
import Routes from './Routes';


export default class App extends Component {
  render() {

    const store = createStore(reducer);

    return (
      <Provider store={store}>
        <View style={styles.container}>
            <StatusBar
              backgroundColor="#1c313a"
              barStyle="light-content"
            />
          <Routes/>
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    flex: 1,
  }
});
