import React, {Component} from 'react'
import {View, Text, StyleSheet, FlatList, ListView, Image, TouchableOpacity, ActivityIndicator } from 'react-native'
import DetailNewsView from './DetailNewsView'
import { NavigationActions } from 'react-navigation'

export default class ViewBerita extends Component {
  state={
    data: [],
    page: 0,
    loading: false,
    refresh: false
  }

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  componentWillMount() {
    this.fetchData();
  }

  fetchData = async() => {
    this.setState({ loading: true });
    const response = await fetch('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Ffeeds.bbci.co.uk%2Fnews%2Ftechnology%2Frss.xml%3Fedition%3Duk');
    const json = await response.json();
    this.setState(state => ({
        data: json.items,
        loading: false,
        loadingHeight: 0,
        refresh: false
    }));
  }

  handleEnd = () => {
    this.setState(state => ({ page: state.page + 1 })),
    this.fetchData();
  }

  showDetail(indexDetail) {
    console.log(indexDetail);
    //<DetailNewsView detail={indexDetail}/>
    this.props.navigation.navigate('Detail', {detail: indexDetail})
  }

  render() {
    return (
      <FlatList
          data={this.state.data}
          keyExtractor={(x, i) => i.toString()}
          onEndReached={() => this.handleEnd()}
          onEndReachedThreshold={0.5}
          refreshing={this.state.refresh}
          onRefresh={() => this.fetchData()}
          style={{backgroundColor: '#fff'}}
          ListFooterComponent={this.renderFooter}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => this.showDetail(item.link)}>
              <View style={styles.row}>
                <Image source={{ uri: item.thumbnail }} style={styles.iconContainer} />
                <View style={styles.info}>
                    <Text style={styles.item}>{`${item.title}`}</Text>
                    <Text style={styles.address}>{`${item.pubDate}`}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
      />
    );
  }
}

const styles = StyleSheet.create({
  row: {
    borderColor: '#9E9E9E',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    flexDirection: 'row',
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 20,
    paddingBottom: 20,
  },
  iconContainer: {
    alignItems: 'center',
    borderColor: '#E0E0E0',
    borderRadius: 2,
    // borderWidth: 1,
    justifyContent: 'center',
    height: 50,
    width: 50,
  },
  icon: {
    tintColor: '#fff',
    height: 22,
    width: 22,
  },
  info: {
    flex: 1,
    paddingLeft: 25,
    paddingRight: 25,
  },
  listStyle: {
    height: 25
  },
  items: {
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 15,
    paddingBottom: 15,
  },
  address: {
    fontSize: 10,
  },
})
