import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PixelRatio,
  TouchableOpacity,
  Image,
  TouchableHighlight,
  FlatList
} from 'react-native';

import ImagePicker from '../../node_modules/react-native-image-picker';

const avatarIcon = require('../images/friends.png')
const cameraIcon = require('../images/camera.png')
const add = require('../images/addStatus.png')
export default class Status extends React.Component {

  state = {
    avatarSource: null,
    videoSource: null,
    data: [1]
  };

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        // let source = { uri: response.uri };

        // You can also display the image using data:
        let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source
        });
      }
    });
  }

  _getFunction(){
    if (this.state.avatarSource === null){
        this.selectPhotoTapped()
    } else {
        this.props.navigation.navigate('StatusDetail', {photoDetail: this.state.avatarSource})
    }
  }

  _getPlus() {
    if (this.state.avatarSource === null){
      return (
        <TouchableOpacity style={styles.plus} onPress={this.selectPhotoTapped.bind(this)}>
            <Image source={add} size={10} style={styles.plusAdd}/>
        </TouchableOpacity>
      )
    }
  }

  render() {
    return (
      <View style={styles.mainContainer}>
          <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
            <View style={styles.rowStatus}>
              <View style={styles.status}>
                <TouchableOpacity style={styles.iconContainerStatus} onPress={this._getFunction.bind(this)}>
                    <Image source={this.state.avatarSource} style={styles.iconContainer}  />
                </TouchableOpacity>
                <View>
                    {this._getPlus()}
                </View>
              </View>
              <View style={styles.infoStatus}>
                  <Text style={styles.textStatus}>My Status</Text>
                  <Text style={styles.addStatus}>Tap to add status update</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.recent}>
              <Text style={styles.recentText}>Recent Update</Text>
          </View>
          <FlatList
          data={this.state.data}
          keyExtractor={(x, i) => i.toString()}
          style={{backgroundColor: '#fff'}}
          renderItem={({item}) => (
              <TouchableOpacity>
                <View style={styles.row}>
                  <TouchableOpacity style={styles.iconContainer} >
                    <Image source={avatarIcon} style={styles.iconContainer}  />
                  </TouchableOpacity>
                  <View style={styles.info}>
                      <Text style={styles.pengguna}>LessNappDev</Text>
                      <Text style={styles.tanggal}>2 Minutes Ago</Text>
                  </View>
                </View>
              </TouchableOpacity>
          )}
        />
    </View>
    );
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  status: {
    flexDirection: 'row',
    paddingRight: 12,
  },
  row: {
    flexDirection: 'row',
  },
  rowStatus: {
    flexDirection: 'row',
  },
  recent: {
    backgroundColor: '#EEEEEE',
    height: 30,
  },
  recentText: {
    borderWidth: 1,
    borderColor: '#E0E0E0',
    justifyContent: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    color: '#000'
  },
  iconContainer: {
    alignItems: 'center',
    borderColor: '#f1f1f1',
    borderRadius: 30,
    borderWidth: 1,
    justifyContent: 'center',
    height: 60,
    width: 60,
    borderWidth: 3,
    margin: 15,
  },
  plus: {
    backgroundColor: '#1c313a',
    alignItems: 'center',
    borderRadius: 10,
    height: 20,
    width: 20,
    marginTop: 53,
    marginLeft: -33,
  },
  plusAdd: {
    alignItems: 'center',
    height: 15,
    width: 15,
    marginTop: 2.5,
  },
  iconContainerStatus: {
    alignItems: 'center',
    borderColor: '#E0E0E0',
    borderRadius: 30,
    justifyContent: 'center',
    height: 60,
    width: 60,
    borderWidth: 3,
    margin: 15,
  },
  icon: {
    tintColor: '#fff',
    height: 22,
    width: 22,
  },
  info: {
    flex: 1,
    paddingLeft: 0,
    paddingRight: 25,
    paddingTop: 20,
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderColor: '#BDBDBD',
  },
  infoStatus: {
    flex: 1,
    paddingLeft: 0,
    paddingRight: 25,
    paddingTop: 20,
    paddingBottom: 20,
  },
  pengguna: {
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 5,
    color: '#000'
  },
  tanggal: {
    color: '#ccc',
    fontSize: 14,
  },
  textStatus: {
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 5,
    color: '#000'
  },
  addStatus: {
    color: '#ccc',
    fontSize: 16,
  },
  date: {
    fontSize: 12,
    marginBottom: 5,
  },
  footer: {
    position: 'absolute',
    alignItems: 'center',
    bottom: 0,
    left: 0,
    right: 0,
  },
  addButton: {
    backgroundColor: '#1c313a',
    width: 55,
    height: 55,
    borderRadius: 50,
    borderColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
    marginBottom: 30,
    marginLeft: 250,
    zIndex: 10,
  },
});
