import React, { Component } from 'react';
import { Image, Text, View, StyleSheet, ScrollView, KeyboardAvoidingView, TextInput, TouchableHighlight, Keyboard, TouchableOpacity, ImageBackground } from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import AutogrowInput from 'react-native-autogrow-input';
import {
  Menu,
  MenuProvider,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';
const back = require('../images/back.png')
const avatarIcon = require('../images/friends.png')
const send = require('../images/near.png')
const more = require('../images/more.png')
//used to make random-sized messages
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// The actual chat view itself- a ScrollView of BubbleMessages, with an InputBar at the bottom, which moves with the keyboard
export default class ChatDetail extends Component {

  constructor(props) {
    super(props);

    var loremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac orci augue. Sed fringilla nec magna id hendrerit. Proin posuere, tortor ut dignissim consequat, ante nibh ultrices tellus, in facilisis nunc nibh rutrum nibh.';

    //create a set number of texts with random lengths. Also randomly put them on the right (user) or left (other person).
    var numberOfMessages = 20;

    var messages = [];

    for(var i = 0; i < numberOfMessages; i++) {
      var messageLength = getRandomInt(10, 120);

      var direction = getRandomInt(1, 2) === 1 ? 'right' : 'left';

      message = loremIpsum.substring(0, messageLength);

      messages.push({
        direction: direction,
        text: message
      })
    }

    this.state = {
      messages: messages,
      inputBarText: ''
    }
  }

  static navigationOptions = {
    title: 'Chat',
  };

  //fun keyboard stuff- we use these to get the end of the ScrollView to "follow" the top of the InputBar as the keyboard rises and falls
  componentWillMount () {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  //
  // //When the keyboard appears, this gets the ScrollView to move the end back "up" so the last message is visible with the keyboard up
  // //Without this, whatever message is the keyboard's height from the bottom will look like the last message.
  keyboardDidShow (e) {
    this.scrollView.scrollToEnd();
  }

  //When the keyboard dissapears, this gets the ScrollView to move the last message back down.
  keyboardDidHide (e) {
    this.scrollView.scrollToEnd();
  }

  //scroll to bottom when first showing the view
  componentDidMount() {
    setTimeout(function() {
      this.scrollView.scrollToEnd();
    }.bind(this))
  }

  //this is a bit sloppy: this is to make sure it scrolls to the bottom when a message is added, but
  //the component could update for other reasons, for which we wouldn't want it to scroll to the bottom.
  componentDidUpdate() {
    setTimeout(function() {
      this.scrollView.scrollToEnd();
    }.bind(this))
  }

  _sendMessage() {
    this.state.messages.push({direction: "right", text: this.state.inputBarText});

    this.setState({
      messages: this.state.messages,
      inputBarText: ''
    });
  }

  _onChangeInputBarText(text) {
    this.setState({
      inputBarText: text
    });
  }

  //This event fires way too often.
  //We need to move the last message up if the input bar expands due to the user's new message exceeding the height of the box.
  //We really only need to do anything when the height of the InputBar changes, but AutogrowInput can't tell us that.
  //The real solution here is probably a fork of AutogrowInput that can provide this information.
  _onInputSizeChange() {
    setTimeout(function() {
      this.scrollView.scrollToEnd({animated: false});
    }.bind(this))
  }

  render() {

    var messages = [];

    this.state.messages.forEach(function(message, index) {
      messages.push(
          <MessageBubble key={index} direction={message.direction} text={message.text}/>
        );
    });

    const params = this.props.navigation.state.params
    const name = params.name

    return (
      <MenuProvider style={{flex: 1}}>
          <View style={styles.headerContainer}>
              <View style={styles.leftHeaderContainer}>
                  <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                      <Image source={back}  size={18} style={styles.icon}  />
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.avatar}>
                      <Image source={avatarIcon} style={styles.avatar}/>
                  </TouchableOpacity>
                      <Text style={styles.logoText}>{name}</Text>
              </View>
          </View>
          <View style={styles.outer}>
            <ImageBackground style={{flex: 1}} source={require('../images/background.png')}>
              <ScrollView ref={(ref) => { this.scrollView = ref }} style={styles.messages}>
                {messages}
              </ScrollView>

              <InputBar onSendPressed={() => this._sendMessage()}
                        onSizeChange={() => this._onInputSizeChange()}
                        onChangeText={(text) => this._onChangeInputBarText(text)}
                        text={this.state.inputBarText}/>

              <KeyboardSpacer style={{height: 50}}/>
              </ImageBackground>
          </View>
        </MenuProvider>
    );
  }
}

//The bubbles that appear on the left or the right for the messages.
class MessageBubble extends Component {
  render() {

    //These spacers make the message bubble stay to the left or the right, depending on who is speaking, even if the message is multiple lines.
    var leftSpacer = this.props.direction === 'left' ? null : <View style={{width: 70}}/>;
    var rightSpacer = this.props.direction === 'left' ? <View style={{width: 70}}/> : null;

    var bubbleStyles = this.props.direction === 'left' ? [styles.messageBubble, styles.messageBubbleLeft] : [styles.messageBubble, styles.messageBubbleRight];

    var bubbleTextStyle = this.props.direction === 'left' ? styles.messageBubbleTextLeft : styles.messageBubbleTextRight;

    return (
            <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
              {leftSpacer}
              <View style={bubbleStyles}>
                <Text style={bubbleTextStyle}>
                  {this.props.text}
                </Text>
              </View>
              {rightSpacer}
            </View>
      );
  }
}

//The bar at the bottom with a textbox and a send button.
class InputBar extends Component {

  //AutogrowInput doesn't change its size when the text is changed from the outside.
  //Thus, when text is reset to zero, we'll call it's reset function which will take it back to the original size.
  //Another possible solution here would be if InputBar kept the text as state and only reported it when the Send button
  //was pressed. Then, resetInputText() could be called when the Send button is pressed. However, this limits the ability
  //of the InputBar's text to be set from the outside.
  componentWillReceiveProps(nextProps) {
    if(nextProps.text === '') {
      this.autogrowInput.resetInputText();
    }
  }

  render() {
    // const params = this.props.navigation.state.params
    // const name = params.name
    return (
          <View style={styles.footer}>
               <View style={styles.SectionStyle}>
                   <AutogrowInput
                   style={{flex:1, fontSize: 18, paddingLeft: 20, paddingRight: 5,}}
                   ref={(ref) => { this.autogrowInput = ref }}
                        multiline={true}
                        defaultHeight={50}
                        onChangeText={(text) => this.props.onChangeText(text)}
                        onContentSizeChange={this.props.onSizeChange}
                        underlineColorAndroid="transparent"
                        value={this.props.text}
                   />
                   <TouchableOpacity onPress={() => this.props.onSendPressed()}>
                     <Text style={styles.text}>Send</Text>
                   </TouchableOpacity>
               </View>
             </View>
          );
  }
}

//TODO: separate these out. This is what happens when you're in a hurry!
const styles = StyleSheet.create({

  //ChatView

  outer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    zIndex: 10,
  },
  icon: {
    tintColor: '#fff',
    height: 20,
    width: 20,
    margin: 4,
    paddingRight: 25,
 },
 menu: {
    fontSize: 17,
    color: '#000',
    paddingLeft: 10,
    margin: 3,
 },
 text: {
    color: '#1264d6',
    fontSize: 18,
    justifyContent: 'center',
    paddingRight: 10,
 },
 avatar: {
    alignItems: 'center',
    borderColor: '#f1f1f1',
    borderRadius: 25,
    borderWidth: 1,
    justifyContent: 'center',
    height: 50,
    width: 50,
    margin: 8,
 },
 logoText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 20,
    alignItems: "flex-start",
    marginTop: 17,
    marginLeft: 3,
 },
  messages: {
    flex: 1
  },

  headerContainer: {
     flexDirection: "row",
     justifyContent: "space-between",
     backgroundColor: "#1c313a",
     alignItems:"center",
     paddingRight: 5,
     height: 60,
  },
  leftHeaderContainer: {
      alignItems: "flex-start",
      flexDirection: "row"
  },
  rightHeaderContainer: {
      alignItems: "flex-end",
      flexDirection: "row"
  },
  icon: {
    tintColor: '#fff',
    justifyContent: 'center',
    margin: 20,
    height: 22,
    width: 22,
  },
  //InputBar

  inputBar: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    paddingHorizontal: 5,
    paddingVertical: 3,
    paddingBottom: 0
  },

  textBox: {
    padding: 10,
    borderRadius: 5,
    height: 20,
    borderColor: 'gray',
    flex: 1,
    fontSize: 16,
    // paddingHorizontal: 10
  },

  sendButton: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    marginLeft: 5,
    paddingRight: 15,
    borderRadius: 5,
    backgroundColor: '#66db30'
  },

  //MessageBubble

  messageBubble: {
      borderRadius: 5,
      marginTop: 8,
      marginRight: 10,
      marginLeft: 10,
      paddingHorizontal: 10,
      paddingVertical: 5,
      flexDirection:'row',
      flex: 1
  },

  messageBubbleLeft: {
    backgroundColor: '#42A5F5',
    margin: 10,
  },

  messageBubbleTextLeft: {
    color: 'white'
  },

  messageBubbleRight: {
    backgroundColor: '#d5d8d4',
    margin: 10,
  },

  messageBubbleTextRight: {
    color: 'black'
  },

  footer: {
       flexDirection: 'row',
       position: 'absolute',
       alignItems: 'center',
       bottom: 0,
       left: 0,
       right: 0,
    },
  SectionStyle: {
       flex: 1,
       flexDirection: 'row',
       justifyContent: 'center',
       alignItems: 'center',
       backgroundColor: '#fff',
       borderWidth: .5,
       borderColor: '#fff',
       height: 40,
       borderRadius: 8 ,
       paddingRight: 5,
       margin: 3,
       elevation: 5,
    },
    text: {
       color: '#1264d6',
       fontSize: 18,
       justifyContent: 'center',
       paddingRight: 10,
    }
})
