
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged } from '../actions/Index';

class Form extends Component {

  onEmailChange(text) {
    this.props.emailChanged(text)
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text)
  }

	render(){
		return(
            <View style={styles.container}>
                <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Email"
                    placeholderTextColor = "#c1cedd"
                    // selectionColor="#fff"
                    keyboardType="email-address"
                    onSubmitEditing={()=> this.password.focus()}
                    onChangeText={this.onEmailChange.bind(this)}
                />
                <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor = "#c1cedd"
                    ref={(input) => this.password = input}
                    onChangeText={this.onPasswordChange.bind(this)}
                />
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText}>{this.props.type}</Text>
                </TouchableOpacity>
            </View>
		)
	}
}

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center'
  },
  inputBox: {
    width:300,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 3,
    paddingHorizontal:16,
    fontSize:16,
    marginVertical: 10,
    borderWidth: 0.9,
  },
  button: {
    width:300,
    borderRadius: 3,
    marginVertical: 10,
    paddingVertical: 13,
    borderColor: "#288cff",
    borderWidth: 0.9,
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#288cff',
    textAlign:'center'
  }
});

const mapStateToProps = ({auth}) => {
  const { email, password } = auth
  return { email, password }
}

export default connect(mapStateToProps,{emailChanged, passwordChanged})(Form);
