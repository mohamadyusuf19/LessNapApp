import React, { Component } from 'react';
import {
 AppRegistry,
 StyleSheet,
 Text,
 View,
 Image,
 TouchableHighlight,
 TouchableOpacity,
 FlatList
} from 'react-native';
import { connect } from 'react-redux';
import ScrollableTabView from 'react-native-scrollable-tab-view'
import {
  Menu,
  MenuProvider,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';
import Member from './Member'
import Status from './Status'
import NewsView from './NewsView'
import ImagePicker from '../../node_modules/react-native-image-picker';

const emoticon = require('../images/emoticon.png')
const back = require('../images/back.png')
const avatarIcon = require('../images/friends.png')
const send = require('../images/near.png')
const more = require('../images/more.png')

class SelectContact extends Component {
 render() {
 return (
   <MenuProvider style={{flex: 1}}>
       <View style={styles.headerContainer}>
           <View style={styles.leftHeaderContainer}>
               <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                   <Image source={back}  size={18} style={styles.back}  />
               </TouchableOpacity>
               <View style={{flexDirection: 'column'}}>
                   <Text style={styles.logoText}>Select Contact</Text>
                   <Text style={{fontSize: 16, color: "white",}}>    75 Contact</Text>
               </View>
           </View>
           <View style={styles.rightHeaderContainer}>
               <Menu onSelect={value => alert(`Selected number: ${value}`)}>
                   <MenuTrigger>
                       <Image source={more} size={18} style={styles.icon} />
                   </MenuTrigger>
                   <MenuOptions>
                       <MenuOption value={1}>
                           <Text style={styles.menu}>Settings</Text>
                       </MenuOption>
                       <MenuOption value={2}>
                           <Text style={styles.menu}>Profile</Text>
                       </MenuOption>
                       <MenuOption value={3}>
                           <Text style={styles.menu}>Help</Text>
                       </MenuOption>
                   </MenuOptions>
               </Menu>
           </View>
       </View>
       <FlatList
       data={this.props.data}
       keyExtractor={(x, i) => i.toString()}
       style={{backgroundColor: '#fff'}}
       renderItem={({item}) => (
         <TouchableOpacity onPress={() => this.onLearnMore(item.title)}>
           <View style={styles.row}>
             <TouchableOpacity style={styles.iconContainer}>
                 <Image source={avatarIcon} style={styles.iconContainer}  />
             </TouchableOpacity>
             <View style={styles.info}>
             <View style={styles.total}>
                 <Text style={styles.pengguna}>{item.title}</Text>
                 <Text style={styles.date}>Mobile</Text>
             </View>
                 <Text style={styles.description}>{item.detail}</Text>
             </View>
           </View>
         </TouchableOpacity>
       )}
     />
   </MenuProvider>
 );
 }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent'
  },
  icon: {
     tintColor: '#fff',
     height: 20,
     width: 20,
     margin: 4,
     paddingRight: 25,
  },
  row: {
    flexDirection: 'row',
    height: 75,
  },
  info: {
    flex: 1,
    paddingLeft: 10,
    paddingTop: 15,
    borderBottomWidth: 1,
    borderColor: '#BDBDBD',
  },
  pengguna: {
    fontWeight: 'bold',
    fontSize: 17,
    color: '#000',
  },
  menu: {
     fontSize: 17,
     color: '#000',
     paddingLeft: 10,
     margin: 3,
  },
  addButton: {
     backgroundColor: 'transparent',
     width: 60,
     height: 30,
     borderRadius: 20,
     borderColor: '#ccc',
  },
  headerContainer: {
     flexDirection: "row",
     justifyContent: "space-between",
     backgroundColor: "#1c313a",
     alignItems:"center",
     paddingRight: 5,
     height: 60,
  },
  back: {
     tintColor: '#fff',
     marginLeft: 14,
     marginTop: 17,
     height: 25,
     width: 25,
     marginRight: 25,
     justifyContent: 'center',
  },
  text: {
     color: '#1264d6',
     fontSize: 18,
     justifyContent: 'center',
     paddingRight: 10,
  },
  avatar: {
     alignItems: 'center',
     borderColor: '#f1f1f1',
     borderRadius: 25,
     borderWidth: 1,
     justifyContent: 'center',
     height: 50,
     width: 50,
     margin: 8,
  },
  logoText: {
     color: "white",
     fontWeight: "bold",
     fontSize: 20,
     alignItems: "flex-start",
     marginTop: 17,
     marginLeft: 3,
  },
  footer: {
     flexDirection: 'row',
     position: 'absolute',
     alignItems: 'center',
     bottom: 0,
     left: 0,
     right: 0,
  },
  leftHeaderContainer: {
     alignItems: "flex-start",
     flexDirection: "row"
  },
  rightHeaderContainer: {
     alignItems: "flex-end",
     flexDirection: "row"
  },
  SectionStyle: {
     flex: 1,
     flexDirection: 'row',
     justifyContent: 'center',
     alignItems: 'center',
     backgroundColor: '#fff',
     borderWidth: .5,
     borderColor: '#fff',
     height: 40,
     borderRadius: 20 ,
     paddingRight: 5,
     margin: 10,
     elevation: 8,
  },
  description: {
    fontSize: 15,
    paddingBottom: 30
  },
  total: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 5,
    paddingLeft: 0,
  },
  date: {
    fontSize: 15,
    paddingRight: 10,
  },
  iconContainer: {
    alignItems: 'center',
    borderColor: '#f1f1f1',
    borderRadius: 28,
    borderWidth: 1,
    justifyContent: 'center',
    height: 56,
    width: 56,
    margin: 10,
  }
});

const mapStateToProps = state => {
  return { data: state.data }
}

export default connect(mapStateToProps)(SelectContact);
