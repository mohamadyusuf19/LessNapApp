import React, {Component} from 'react'
import {View, Text, StyleSheet, TextInput, Image,} from 'react-native'
const searchIcon = require('../images/searchBlack.jpg')
export default class Searchbar extends Component {

  render(){

    return(
      <View style={styles.container}>
        <View style={styles.SectionStyle}>
          <Image source={searchIcon} style={styles.ImageStyle}/>
            <TextInput
            autoFocus = {true}
            style={{flex:1}}
            placeholder= "Search"
            placeholderTextColor= "#BDBDBD"
            underlineColorAndroid= 'transparent'
            />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    paddingLeft: 0,
    paddingRight: 0,
    paddingVertical: 2,
    backgroundColor: 'transparent',
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: .5,
    borderColor: '#fff',
    height: 40,
    borderRadius: 5 ,
    margin: 10,
    elevation: 8,
  },
  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode : 'stretch',
    alignItems: 'center'
  },
})
