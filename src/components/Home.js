import React, { Component } from 'react';
import {
 AppRegistry,
 StyleSheet,
 Text,
 View,
 Image,
 TouchableHighlight,
 TouchableOpacity,

} from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view'
import {
  Menu,
  MenuProvider,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';
import Member from './Member'
import Status from './Status'
import NewsView from './NewsView'
import ImagePicker from '../../node_modules/react-native-image-picker';

const search = require('../images/search.png')
const add = require('../images/add.png')
const more = require('../images/more.png')
const camera = require('../images/camera.png')
const message = require('../images/create.png')
const share = require('../images/share.png')
export default class Home extends Component {
  constructor(props) {
      super(props)
      this.state= {
        selected: 0,
      }
    }

  handleChangeTab(index) {
      this.setState({selected: index.i});
  }
  state = {
    avatarSource: null,
    videoSource: null,
    data: [1]
  };

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        // let source = { uri: response.uri };

        // You can also display the image using data:
        let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source
        });
      }
    });
  }

  _getIcon() {
    if(this.state.selected === 0) {
        return add
    } else if (this.state.selected === 1) {
        return camera
    }
  }

    _getTouchable() {
      if(this.state.selected === 0) {
          return (
            <TouchableHighlight style={styles.addButton} onPress={this._getFunction.bind(this)}>
              <Image source={this._getIcon()} size={20} style={styles.icon} />
            </TouchableHighlight>
          )
      } else if (this.state.selected === 1) {
          return (
            <TouchableHighlight style={styles.addButton} onPress={this._getFunction.bind(this)}>
              <Image source={this._getIcon()} size={20} style={styles.icon} />
            </TouchableHighlight>
          )
      }

  }

  onLearnMore = () => {
      this.props.navigation.navigate('SelectContact');
  };

  onLearnSearch = () => {
      this.props.navigation.navigate('Search');
  };

  _getFunction() {
    if(this.state.selected == 0) {
      this.onLearnMore()
    } else {
      this.selectPhotoTapped()
    }
  }


 render() {
 return (
  <MenuProvider style={{flex: 1}}>
   <View style={styles.mainContainer}>
      <View style={styles.headerContainer}>
        <View style={styles.leftHeaderContainer}>
            <Text style={styles.logoText}>LessNap</Text>
        </View>
        <View style={styles.rightHeaderContainer}>
          <TouchableOpacity onPress={() => this.onLearnSearch()}>
            <Image source={search} size={18} style={styles.icon} />
          </TouchableOpacity>
          <Menu onSelect={value => alert(`Selected number: ${value}`)}>
              <MenuTrigger>
                  <Image source={more} size={18} style={styles.icon} />
              </MenuTrigger>
              <MenuOptions>
                  <MenuOption value={1}>
                      <Text style={styles.menu}>Settings</Text>
                  </MenuOption>
                  <MenuOption value={2}>
                      <Text style={styles.menu}>Profile</Text>
                  </MenuOption>
                  <MenuOption value={3}>
                      <Text style={styles.menu}>Help</Text>
                  </MenuOption>
              </MenuOptions>
          </Menu>
        </View>
        </View>
        <View style={styles.contentContainer}>
          <ScrollableTabView
              onChangeTab={(index) => this.handleChangeTab(index)}
              tabBarUnderlineColor="#fff"
              tabBarUnderlineStyle={{backgroundColor: "#fff"}}
              tabBarBackgroundColor ="#1c313a"
              tabBarActiveTextColor="#fff"
              tabBarInactiveTextColor="#88b0ac"
              >
             <Member tabLabel="CHATS" {...this.props} />
             <Status tabLabel="STATUS" {...this.props} />
             <NewsView tabLabel="NEWS" {...this.props} />
         </ScrollableTabView>
        </View>
        <View style={styles.footer}>
            {this._getTouchable()}
        </View>
     </View>
  </MenuProvider>
 );
 }
}
const styles = StyleSheet.create({
 mainContainer: {
    flex: 1,
    backgroundColor: '#F5FCFF',
 },
 icon: {
    tintColor: '#fff',
    height: 20,
    width: 20,
    margin: 4,
    paddingRight: 25,
 },
 menu: {
    fontSize: 17,
    color: '#000',
    paddingLeft: 10,
    margin: 3,
 },
 footer: {
    position: 'absolute',
    alignItems: 'center',
    bottom: 20,
    right: 20,
    justifyContent: 'flex-end',
    flexGrow: 2,
    flex: 1,
  },
  addButton: {
    position: 'absolute',
    backgroundColor: '#1c313a',
    alignSelf: 'flex-end',
    width: 55,
    height: 55,
    borderRadius: 50,
    borderColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center',
  },
 headerContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#1c313a",
    alignItems:"center",
    paddingRight: 5
 },
 leftHeaderContainer: {
    alignItems: "flex-start",
    flexDirection: "row"
 },
 rightHeaderContainer: {
    alignItems: "flex-end",
    flexDirection: "row"
 },
 contentContainer: {
    flex: 9,
 },
 logoText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 20,
    alignItems: "flex-start",
    paddingLeft: 20
 },
});
