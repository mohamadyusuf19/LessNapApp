//bug BackHandler adalah ketika button back di klik interval belum selesai.
import React, {Component} from 'react'
import {View, Text, StyleSheet, Image, Modal, TouchableOpacity, ProgressBarAndroid, BackHandler, Alert, Platform} from 'react-native'
import {
  Menu,
  MenuProvider,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';
const cameraIcon = require('../images/camera.png')
const back = require('../images/back.png')
const avatarIcon = require('../images/friends.png')
const send = require('../images/near.png')
const more = require('../images/more.png')

class StatusDetail extends Component {
  constructor(props){
     super(props)
     this.state = {
       numberOfReferesh: 0.00,
       statusInterval: null,
     };
   }
   componentWillMount(){
     this.interval = setInterval(
     () => {
       if(this.state.numberOfReferesh < 1) {
         this.setState(prevState => ({
           numberOfReferesh: prevState.numberOfReferesh + 0.01
         }))
       }
       else {
         clearInterval(this.interval)
         return this.props.navigation.goBack()
       }
     }, 40);
     const params = this.props.navigation.state.params
     this.setState({statusInterval: params.photoDetail});
     BackHandler.removeEventListener("hardwareBackPress", this.onBackClicked);
   }

   componentWillUnmount() {
       if (Platform.OS === 'android') {

         BackHandler.removeEventListener("hardwareBackPress", clearInterval(this.interval));
       }
   }

   // _onBackClicked = () => {
   //      clearInterval(this.interval)
   // }

   checkInterval() {
     if (this.state.numberOfReferesh < 1 ) {
       return this.state.statusInterval
     }
   }

   // componentDidMount = () => {
   //    BackHandler.removeEventListener('hardwareBackPress', this.navigationBack.bind(this))
   // }
   //
   // navigationBack() {
   //   BackHandler.exitApp()
   //   return true
   // }

   render(){

    const params = this.props.navigation.state.params
    const _photoDetail = params.photoDetail
    const textToDisplay = `Gambar akan menutup pada: ${this.state.numberOfReferesh}`
    return(
        <MenuProvider style={{flex: 1}}>
            <View style={styles.container}>
              <ProgressBarAndroid styleAttr='Horizontal' progress={this.state.numberOfReferesh}
                indeterminate={false} style={styles.progressBar}
               />
               <View style={styles.headerContainer}>
                   <View style={styles.leftHeaderContainer}>
                       <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                           <Image source={back}  size={18} style={styles.back}  />
                       </TouchableOpacity>
                       <TouchableOpacity style={styles.avatar}>
                           <Image source={avatarIcon} style={styles.avatar}/>
                       </TouchableOpacity>
                           <Text style={styles.logoText}>LessNap</Text>
                   </View>
                   <View style={styles.rightHeaderContainer}>
                       <Menu onSelect={value => alert(`Selected number: ${value}`)}>
                           <MenuTrigger>
                               <Image source={more} size={18} style={styles.icon} />
                           </MenuTrigger>
                           <MenuOptions>
                               <MenuOption value={1}>
                                   <Text style={styles.menu}>Settings</Text>
                               </MenuOption>
                               <MenuOption value={2}>
                                   <Text style={styles.menu}>Profile</Text>
                               </MenuOption>
                               <MenuOption value={3}>
                                   <Text style={styles.menu}>Help</Text>
                               </MenuOption>
                           </MenuOptions>
                       </Menu>
                   </View>
               </View>

              <Image source={this.checkInterval()} style={styles.iconContainer}/>
        </View>
      </MenuProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#000',
    top: 0,
    right: 0,
    left: 0,
  },
  rightHeaderContainer: {
     alignItems: "flex-end",
     flexDirection: "row"
  },
  logoText: {
     color: "white",
     fontWeight: "bold",
     fontSize: 20,
     alignItems: "flex-start",
     marginTop: 17,
     marginLeft: 3,
  },
  back: {
     tintColor: '#fff',
     marginLeft: 14,
     marginTop: 17,
     height: 25,
     width: 25,
     marginRight: 25,
     justifyContent: 'center',
  },
  avatar: {
     alignItems: 'center',
     borderColor: '#f1f1f1',
     borderRadius: 25,
     borderWidth: 1,
     justifyContent: 'center',
     height: 50,
     width: 50,
     margin: 8,
  },
  leftHeaderContainer: {
     alignItems: "flex-start",
     flexDirection: "row",
  },
  headerContainer: {
     flexDirection: "row",
     justifyContent: "space-between",
     backgroundColor: "transparent",
     alignItems:"center",
     paddingRight: 5,
     height: 80,
  },
  icon: {
     tintColor: '#fff',
     height: 20,
     width: 20,
     margin: 4,
     paddingRight: 25,
  },
  menu: {
     fontSize: 17,
     color: '#000',
     paddingLeft: 10,
     margin: 3,
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 400,
    width: 370,
    backgroundColor: '#000'
  },
  progressBar: {
    alignItems: 'flex-start',
    width: 380,
    borderRadius: 20,
    backgroundColor: '#000'
  },
});

export default StatusDetail;
