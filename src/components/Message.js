import React from 'react';
import { View, StyleSheet, Text } from 'react-native'

const Message = ({ item }) => (
  <View style={[
      styles.message, item.incoming &&
      styles.incomingMessage
    ]}>
    <Text>{item.message}</Text>
  </View>
)

const styles = {
  message: {
    width: '70%',
    margin: 10,
    padding: 10,
    backgroundColor: 'white',
    borderStyle: 'solid',
    borderRadius: 30
  },
  incomingMessage: {
    alignSelf: 'flex-end',
    backgroundColor: '#1c313a'
  }
}

export default Message;
