import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, ListView, FlatList, ActivityIndicator, TouchableOpacity, Modal } from 'react-native';
import ChatDetail from './ChatDetail';
import { connect } from 'react-redux';

const avatarIcon = require('../images/friends.png')
const writeIcon = require('../images/create.png')
class Member extends Component { // extends adalah inheritence concept

  state = {
    modalVisible: false,
  };

  onLearnMore = (item) => {
      this.props.navigation.navigate('Chat', {name: item});
  };

  render(){
    return(
      <View style={styles.mainContainer}>
          <FlatList
          data={this.props.data}
          keyExtractor={(x, i) => i.toString()}
          style={{backgroundColor: '#fff'}}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => this.onLearnMore(item.title)}>
              <View style={styles.row}>
                <TouchableOpacity style={styles.iconContainer}>
                    <Image source={avatarIcon} style={styles.iconContainer}  />
                </TouchableOpacity>
                <View style={styles.info}>
                <View style={styles.total}>
                    <Text style={styles.pengguna}>{item.title}</Text>
                    <Text style={styles.date}>Yesterday</Text>
                </View>
                    <Text style={styles.description}>{item.detail}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  row: {
    flexDirection: 'row',
    height: 75,
  },
  info: {
    flex: 1,
    paddingLeft: 10,
    paddingTop: 15,
    borderBottomWidth: 1,
    borderColor: '#BDBDBD',
  },
  pengguna: {
    fontWeight: 'bold',
    fontSize: 17,
    color: '#000',
  },
  description: {
    fontSize: 15,
    paddingBottom: 30
  },
  footer: {
    position: 'absolute',
    alignItems: 'center',
    bottom: 0,
    left: 0,
    right: 0,
  },
  iconContainer: {
    alignItems: 'center',
    borderColor: '#f1f1f1',
    borderRadius: 28,
    borderWidth: 1,
    justifyContent: 'center',
    height: 56,
    width: 56,
    margin: 10,
  },
  icon: {
    tintColor: '#fff',
    height: 22,
    width: 22,
  },
  total: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 5,
    paddingLeft: 0,
  },
  date: {
    fontSize: 15,
    paddingRight: 10,
  },
  addButton: {
    backgroundColor: '#1c313a',
    width: 55,
    height: 55,
    borderRadius: 50,
    borderColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
    marginBottom: 30,
    marginLeft: 250,
    zIndex: 10,
  },
})

const mapStateToProps = state => {
  return { data: state.data }
}

export default connect(mapStateToProps)(Member);
