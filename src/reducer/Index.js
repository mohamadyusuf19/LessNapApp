import { combineReducers } from 'redux'
import LoginReducer from './LoginReducer';
import DataUserReducer from './DataUserReducer';

export default combineReducers({
    auth: LoginReducer,
    data: DataUserReducer
});
