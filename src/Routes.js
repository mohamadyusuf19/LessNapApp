import React, { Component } from 'react';
import {
  StackNavigator,
} from 'react-navigation';
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition';
import Login from './pages/Login';
import Home from './components/Home'
import Member from './components/Member'
import Searchbar from './components/Searchbar'
import DetailNewsView from './components/DetailNewsView'
import StatusDetail from './components/StatusDetail'
import ChatDetail from './components/ChatDetail'
import Status from './components/Status'
import SelectContact from './components/SelectContact'
const Routes = StackNavigator({
  Login: {
		screen: Login,
		navigationOptions: {
        	title: 'Login',
		},
	},
	Home: {
		screen: Home,
		navigationOptions: {
			header: null
		},
	},
  Status: {
		screen: Status,
		navigationOptions: {
			header: null
		},
	},
  Member: {
		screen: Member,
		navigationOptions: {
			header: null
		},
	},
	Detail: {
		screen: DetailNewsView,
		navigationOptions: {
			header: null
		},
	},
	Search: {
		screen: Searchbar,
		navigationOptions: {
			header: null
		},
	},
	StatusDetail : {
		screen: StatusDetail,
		navigationOptions: {
			header: null
		},
	},
  SelectContact : {
    screen: SelectContact,
    navigationOptions: {
      header: null
    },
  },
	Chat: {
		screen: ChatDetail,
		navigationOptions: {
			header: null
		},
	},
}, {
		initialRouteName: 'Home',
    transitionConfig: getSlideFromRightTransition
});

export default Routes
