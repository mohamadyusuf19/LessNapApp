import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar ,
  TouchableOpacity
} from 'react-native';

import Form from '../components/Form';
// import Logo from '../components/Logo'

export default class Login extends Component {


	render() {
		return(
			<View style={styles.container}>
				<Form type="Login"/>
				<View style={styles.signupTextCont}>
					<Text style={styles.signupText}>Dont have an account yet?</Text>
					<TouchableOpacity><Text style={styles.signupButton}> Signup</Text></TouchableOpacity>
				</View>
			</View>
			)
	}
}
const styles = StyleSheet.create({
  container : {
    backgroundColor:'#FFFF',
    flex: 1,
    alignItems:'center',
    justifyContent :'center'
  },
  signupTextCont : {
  	flexGrow: 1,
    alignItems:'flex-end',
    justifyContent :'center',
    paddingVertical:16,
    flexDirection:'row'
  },
  signupText: {
  	color:'#000000',
  	fontSize:16
  },
  signupButton: {
  	color:'#3897F0',
  	fontSize:16,
  	fontWeight:'500'
  }
});
